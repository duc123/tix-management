import * as actions from '../action/taiKhoanAction/actionTypes';

const initialState = {
    danhSachTaiKhoan:[],
    searchTerm:"",
}

export default function taiKhoanReducer(state=initialState, action) {
    const {type, payload} = action;
    switch(type) {
        case actions.FETCH_DANH_SACH_TAI_KHOAN:return{...state, danhSachTaiKhoan:payload};
        case actions.UPDATE_SEARCH_TERM :return {...state, searchTerm:payload};
        default: return state;
    }
}