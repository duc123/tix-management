import { combineReducers } from "redux";
import commonReducer from './commonReducer';
import taiKhoanReducer from './taiKhoanReducer';


export default combineReducers({
    commonReducer,
    taiKhoanReducer,
});
