import React from "react";
import { useSelector } from "react-redux";

const Loading = () => {
  const commonReducer = useSelector((state) => state.commonReducer);
  const { loading } = commonReducer;
  return loading ? (
    <div className="loading">
      <div class="loader-container">
        <div class="loader load-1"></div>
        <div class="loader load-2"></div>
        <div class="loader load-3"></div>
      </div>
    </div>
  ) : (
    ""
  );
};

export default Loading;
