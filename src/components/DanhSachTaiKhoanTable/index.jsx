import { Delete, Edit } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import { handleAlert } from "../../redux/action/CommonAction/actions";

const DanhSachTaiKhoanTable = ({ dataList }) => {
  const commonReducer = useSelector((state) => state.commonReducer);

  const { isLight } = commonReducer;

  const dispatch = useDispatch();

  return (
    <div className="table-wrapper">
      <table className={!isLight ? "dark" : ""}>
        <thead>
          <tr class="row100 head">
            <th>Tai khoan</th>
            <th>Ho ten</th>
            <th>Email</th>
            <th>So dt</th>
            <th>Mat khau</th>
            <th>Ma loai nguoi dung</th>
            <th>Hanh dong</th>
          </tr>
        </thead>
        <tbody>
          {dataList.map((item, idx) => (
            <tr key={idx} class="row100 body">
              <td>{item.taiKhoan}</td>
              <td>{item.hoTen}</td>
              <td>{item.email}</td>
              <td>{item.soDt}</td>
              <td>{item.matKhau}</td>
              <td>{item.maLoaiNguoiDung}</td>
              <td>
                <button
                  onClick={() =>
                    dispatch(handleAlert({ text: "Hello", status: "error" }))
                  }
                  className="btn_sm btn_primary"
                >
                  <Edit fontSize="small" />
                </button>
                <button className="btn_sm btn_primary">
                  <Delete fontSize="small" />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default DanhSachTaiKhoanTable;
