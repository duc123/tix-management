import { ArrowDropDown, ArrowDropUp, Settings } from "@material-ui/icons";
import React, { useState } from "react";
import { NavLink } from "react-router-dom";

const SideBarItem = ({ item }) => {
  const { items } = item;
  const [showItem, setShowItem] = useState(false);

  const formatURL = (url) => {
    const newUrl = url
      .split("")
      .filter((el) => el !== " ")
      .reduce((a, b) => (a += b));
    return newUrl;
  };

  formatURL("Danh sach phim");

  return (
    <div className="sideBar-list-item">
      <div onClick={() => setShowItem(!showItem)} className="item-title">
        <span>{item.title}</span>
        <span className="icon">
          {showItem ? <ArrowDropUp /> : <ArrowDropDown />}
        </span>
      </div>
      <div className={`item-content ${showItem ? "active" : ""}`}>
        {items.map((el, idx) => (
          <NavLink
            to={`/${formatURL(el)}`}
            className="item-content-item"
            key={idx}
          >
            {el}
            <span className="icon">
              <Settings fontSize="small" />
            </span>
          </NavLink>
        ))}
      </div>
    </div>
  );
};

export default SideBarItem;
