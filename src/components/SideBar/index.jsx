import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Close } from "@material-ui/icons";
import { toggleSideBar } from "../../redux/action/CommonAction/actions";
import SideBarItem from "../SideBarItem";

const SideBar = () => {
  const sideBarItemArr = [
    { title: "Quan ly tai khoan", items: ["Danh sach tai khoan"] },
    {
      title: "Quan ly phim",
      items: ["Danh sach phim", "Thong tin phim", "Them phim"],
    },
    { title: "Quan ly rap", items: ["He thong rap", "Lich chieu"] },
    { title: "Quan ly ve", items: ["Danh sach phong ve"] },
  ];

  const dispatch = useDispatch();
  const commonReducer = useSelector((state) => state.commonReducer);
  const { sideBar } = commonReducer;

  return (
    <div className={` sideBar ${sideBar ? "sideBar_active" : ""}`}>
      <div className="sideBar-list">
        <div className="sideBar-list-item">
          <Close
            className="closeBar"
            onClick={() => dispatch(toggleSideBar())}
          />
        </div>
        {sideBarItemArr.map((item) => (
          <SideBarItem item={item} />
        ))}
      </div>
    </div>
  );
};

export default SideBar;
