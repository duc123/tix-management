import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import DanhSachTaiKhoanTable from "../../components/DanhSachTaiKhoanTable";
import SearchUser from "../../components/SearchUser";
import { fetchDanhSachTaiKhoan } from "../../redux/action/taiKhoanAction/actions";

const DanhSachTaiKhoan = () => {
  const dispatch = useDispatch();

  const taiKhoanReducer = useSelector((state) => state.taiKhoanReducer);

  const commonReducer = useSelector((state) => state.commonReducer);
  const { isLight } = commonReducer;

  const { danhSachTaiKhoan, searchTerm } = taiKhoanReducer;

  useEffect(() => {
    dispatch(fetchDanhSachTaiKhoan());
  }, [dispatch]);

  const formatDanhSachTaiKhoan = (arr) => {
    return arr?.filter(
      (el) =>
        el.taiKhoan?.toUpperCase().indexOf(searchTerm.toUpperCase()) > -1 ||
        el.hoTen?.toUpperCase().indexOf(searchTerm.toUpperCase()) > -1 ||
        el.email?.toUpperCase().indexOf(searchTerm.toUpperCase()) > -1
    );
  };

  return (
    <div className={`danhSachTaiKhoan ${!isLight ? "dark" : ""}`}>
      <div className="w-100 danhSachTaiKhoan-actions d-flex flex-row justify-between align-center">
        <SearchUser />
      </div>
      {danhSachTaiKhoan ? (
        <DanhSachTaiKhoanTable
          dataList={formatDanhSachTaiKhoan(danhSachTaiKhoan)}
        />
      ) : (
        ""
      )}
    </div>
  );
};

export default DanhSachTaiKhoan;
