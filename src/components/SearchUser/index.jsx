import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateSearchTerm } from "../../redux/action/taiKhoanAction/actions";

const SearchUser = () => {
  const taiKhoanReducer = useSelector((state) => state.taiKhoanReducer);

  const { searchTerm } = taiKhoanReducer;

  const dispatch = useDispatch();

  return (
    <input
      type="text"
      value={searchTerm}
      onChange={(e) => dispatch(updateSearchTerm(e.target.value))}
      placeholder="Search user"
    />
  );
};

export default SearchUser;
