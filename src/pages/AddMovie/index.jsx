import React from "react";
import { useSelector } from "react-redux";
import { useForm } from "react-hook-form";

const AddMovie = () => {
  const commonReducer = useSelector((state) => state.commonReducer);

  const { errors, register, handleSubmit } = useForm();

  const { isLight } = commonReducer;

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <div className={`addMovie shadow_sm ${!isLight ? "dark" : ""}`}>
      <h1>Add movie</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form-control">
          <label>Ma phim</label>
          <input
            ref={register({ required: true })}
            className={errors.maPhim ? "error" : ""}
            type="text"
            name="maPhim"
          />
          <small>
            {errors.maPhim && errors.maPhim.type === "required"
              ? "Ma phim khong duoc bo trong"
              : ""}
          </small>
        </div>
        <div className="form-control">
          <label>Ten phim</label>
          <input
            ref={register({ required: true })}
            type="text"
            name="tenPhim"
            className={errors.tenPhim ? "error" : ""}
          />
          <small>
            {errors.tenPhim && errors.tenPhim.type === "required"
              ? "Ten phim khong duoc bo trong"
              : ""}
          </small>
        </div>
        <div className="form-control">
          <label>Bi danh</label>
          <input
            ref={register({ required: true })}
            className={errors.biDanh ? "error" : ""}
            type="text"
            name="biDanh"
          />
          <small>
            {errors.biDanh && errors.biDanh.type === "required"
              ? "Bi danh khong duoc bo trong"
              : ""}
          </small>
        </div>
        <div className="form-control">
          <label>Trailer</label>
          <input
            ref={register({ required: true })}
            type="text"
            name="trailer"
            className={errors.trailer ? "error" : ""}
          />
          <small>
            {errors.trailer && errors.trailer.type === "required"
              ? "Trailer khong duoc bo trong"
              : ""}
          </small>
        </div>
        <div className="form-control">
          <label>Hinh anh</label>
          <input
            ref={register({ required: true })}
            type="text"
            name="hinhAnh"
            className={errors.hinhAnh ? "error" : ""}
          />
          <small>
            {errors.hinhAnh && errors.hinhAnh.type === "required"
              ? "Hinh anh khong duoc bo trong"
              : ""}
          </small>
        </div>
        <div className="form-control">
          <label>Mo ta</label>
          <input
            ref={register({ required: true })}
            className={errors.moTa ? "error" : ""}
            type="text"
            name="moTa"
          />
          <small>
            {errors.moTa && errors.moTa.type === "required"
              ? "Mo ta khong duoc bo trong"
              : ""}
          </small>
        </div>
        <div className="form-control">
          <label>Ma nhom</label>
          <input
            ref={register({ required: true })}
            className={errors.maNhom ? "error" : ""}
            type="text"
            name="maNhom"
          />
          <small>
            {errors.maNhom && errors.maNhom.type === "required"
              ? "Ma nhom khong duoc bo trong"
              : ""}
          </small>
        </div>
        <div className="form-control">
          <label>Ngay khoi chieu</label>
          <input
            ref={register({ required: true })}
            type="text"
            name="ngayKhoiChieu"
            className={errors.ngayKhoiChieu ? "error" : ""}
          />
          <small>
            {errors.ngayKhoiChieu && errors.ngayKhoiChieu.type === "required"
              ? "Ngay khoi chieu khong duoc bo trong"
              : ""}
          </small>
        </div>
        <div className="form-control">
          <label>Danh gia</label>
          <textarea
            ref={register({ required: true })}
            type="text"
            name="danhGia"
            className={errors.danhGia ? "error" : ""}
          />
          <small>
            {errors.danhGia && errors.danhGia.type === "required"
              ? "Danh gia khong duoc bo trong"
              : ""}
          </small>
        </div>
        <div className="form-control">
          <button className="btn_lg btn_primary">Add</button>
        </div>
      </form>
    </div>
  );
};

export default AddMovie;
