import axios from 'axios';
import * as actions from './actionTypes';

const fetchDanhSachTaiKhoan = () => async dispatch => {
    try{
        const res = await axios({
            method:"GET",
            url:"https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP09"
        })
        if(res.status === 200 || res.status === 201) {
            await dispatch({
                type: actions.FETCH_DANH_SACH_TAI_KHOAN,
                payload: res.data,
            })
        }
    }catch(err) {
        console.log(err);
    }
}

const updateSearchTerm = (term) => {
    return{
        type: actions.UPDATE_SEARCH_TERM,
        payload: term,
    }
}

export {fetchDanhSachTaiKhoan, updateSearchTerm}