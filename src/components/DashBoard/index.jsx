import React from "react";
import { useSelector } from "react-redux";
import Header from "../Header";
import SideBar from "../SideBar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import DanhSachTaiKhoan from "../../pages/DanhSachTaiKhoan";
import AlertList from "../AlertList";
import AddMovie from "../../pages/AddMovie";
import Loading from "../Loading";

const DashBoard = () => {
  const commonReducer = useSelector((state) => state.commonReducer);
  const { isLight } = commonReducer;
  return (
    <Router>
      <div className={`dashBoard ${!isLight ? "dark" : ""}`}>
        <SideBar />
        <AlertList />
        <Loading />
        <div className="main">
          <Header />
          <Switch>
            <Route exact path="/Danhsachtaikhoan">
              <DanhSachTaiKhoan />
            </Route>
            <Route exact path="/Themphim">
              <AddMovie />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
};

export default DashBoard;
