import React from 'react';
import { Provider } from 'react-redux';
import DashBoard from './components/DashBoard';
import store from './redux/store';


const App = () => {
  return (
    <Provider store={store}>
      
      <DashBoard/>

    </Provider>
  );
}
 
export default App;