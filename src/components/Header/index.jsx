import React from "react";
import { Brightness2, Menu, WbSunny } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import {
  switchLight,
  toggleSideBar,
} from "../../redux/action/CommonAction/actions";
import { NavLink } from "react-router-dom";

const Header = () => {
  const commonReducer = useSelector((state) => state.commonReducer);
  const dispatch = useDispatch();
  const { isLight } = commonReducer;
  return (
    <div className="header">
      <div className="header-title">
        <Menu onClick={() => dispatch(toggleSideBar())} className="menuBar" />
        <h2>
          <NavLink className="link" to="/">
            Dashboard
          </NavLink>
        </h2>
      </div>
      <div className="header-actions">
        <span onClick={() => dispatch(switchLight())} className="toggle-icon">
          {isLight ? <Brightness2 /> : <WbSunny />}
        </span>
      </div>
    </div>
  );
};

export default Header;
